package reclamation.guru.DTO;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;

/**
 * Entity implementation class for Entity: Reclamation
 *
 */


public class Reclamation implements Serializable {

	  
	
	public int Id;
	public String Nom; 
	public String Prenom;
	public int Telephone;
	public String Type;
	public String Email;
	private static final long serialVersionUID = 1L;

	public Reclamation() {
		super();
	} 
	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}
	public String getNom() {
		return this.Nom;
	}

	public void setNom(String Nom) {
		this.Nom = Nom;
	}
	public String getPrenom() {
		return this.Prenom;
	}

	public void setPrenom(String Prenom) {
		this.Prenom = Prenom;
	}  
	public int getTelephone() {
		return this.Telephone;
	}

	public void setTelephone(int Telephone) {
		this.Telephone = Telephone;
	}
	public String getType() {
		return this.Type;
	}

	public void setType(String Type) {
		this.Type = Type;
	}   
	public String getEmail() {
		return this.Email;
	}

	public void setEmail(String Email) {
		this.Email = Email;
	} 
	@Override
	public String toString() {
		return "Reclamation [Id=" + Id + ", Nom=" + Nom + ", Prenom=" + Prenom + ", Telephone=" + Telephone
				+ ", Type=" + Type + ", Email=" + Email + "]";
	}
	public Reclamation( String nom, String prenom, int telephone, String type, String email) {
		
		Nom = nom;
		Prenom = prenom;
		Telephone = telephone;
		Type = type;
		Email = email;
		
	}
	public Reclamation(int id, String nom, String prenom, int telephone, String type, String email) {
		super();
		Id = id;
		Nom = nom;
		Prenom = prenom;
		Telephone = telephone;
		Type = type;
		Email = email;
		
	}
}	