package reclamation.guru.interfaces;


import java.util.List;

import javax.ejb.Local;

import reclamation.guru.DTO.Reclamation;




@Local
public interface ReclamationServiceLocal {
	
	
	List<Reclamation> GetAll();
	public void Delete(int Id);
	public void Create(Reclamation p);
	public void Update(int id,Reclamation p);

	
	
}