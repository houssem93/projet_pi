package reclamation.guru.interfaces;

import java.util.List;

import javax.ejb.Remote;

import reclamation.guru.DTO.Reclamation;




@Remote
public interface ReclamationServiceRemote {

	List<Reclamation> GetAll();
	public void Delete(Reclamation Id);
	public void Create(Reclamation p);
	public void Update(int id,Reclamation p);
	
	
}