package reclamation.guru.services;

import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;
import javax.persistence.EntityManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import reclamation.guru.DTO.Reclamation;
import reclamation.guru.interfaces.ReclamationServiceRemote;


@Stateful
@LocalBean
public class ReclamationService implements ReclamationServiceRemote {
	
	public String GlobalEndPoint = "localhost:44361";
	EntityManager em ;
	@Override
	public List<Reclamation> GetAll() {
		//Client client = ClientBuilder.newClient();
		//WebTarget target = client.target("http://localhost:31618/api/PubWebApi/");
		//WebTarget hello =target.path("");
		//Response response =hello.request().get();
		
		//String result=response.readEntity(String.class);
		
		//PublicationDTO[] pubs =  response.readEntity(PublicationDTO[].class);

		//response.close();
		//return pubs;
		List<Reclamation>  lasp = new ArrayList<Reclamation>();
    	Client client = ClientBuilder.newClient();
    	
    	WebTarget web = client.target("http://localhost:44361/api/reclamations/"); 
    	
    	Response response = web.request().get();
    	
    	String result = response.readEntity(String.class); 
    	
    	//System.out.println(result);
    	JsonReader jsonReader = Json.createReader(new StringReader(result));
    	JsonArray object =  jsonReader.readArray();
    	
    	 
    	for (int i=0;i<object.size();i++)
    	{
    	 
    		Reclamation m = new Reclamation();
    	 //String dateString;
       	 m.setId(object.getJsonObject(i).getInt("Id")); 
    	 m.setNom(object.getJsonObject(i).getString("Nom")); 
    	 m.setPrenom(object.getJsonObject(i).getString("Prenom")); 
    	 m.setTelephone(object.getJsonObject(i).getInt("Telephone")); 
    	 m.setType(object.getJsonObject(i).getString("Type"));
    	 m.setEmail(object.getJsonObject(i).getString("Email"));
    	
    	
    	 lasp.add(m);
    	}
    	

return lasp;    	
	}

	

	
	
	
	
	
	
	
	



	@Override
	public void Delete(Reclamation Id) {
		// TODO Auto-generated method stub
		Client cl = ClientBuilder.newClient();
		WebTarget target = cl.target("http://"+GlobalEndPoint+"/api/reclamations/delete?id="+Id.getId()); 
		WebTarget hello = target.path("");     	
    	Response res=(Response) hello.request().delete();
	}



	@Override
	public void Create(Reclamation p) {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target("http://"+GlobalEndPoint+"/api/reclamations/create");
		WebTarget hello =target.path("");
		
		Response response =hello.request().post(Entity.entity(p, MediaType.APPLICATION_JSON) );
		
		
		String result=response.readEntity(String.class);
		System.out.println(result);
		
		

		response.close();		
		
	}



	@Override
	public void Update(int id, Reclamation p) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				Reclamation e = new Reclamation();
				e.setId(id);
				e.setNom(p.Nom);
				e.setPrenom(p.Prenom);
				e.setTelephone(p.Telephone);
				e.setType(p.Type);
		       e.setEmail(p.Email);
		      
		        
		  		System.out.println("iddddddddd"+id);
		  		System.out.println("OK");

				Client client = ClientBuilder.newClient();
				WebTarget target = client.target("http://"+GlobalEndPoint+"/api/reclamations/put?id="+id);
				Response response = target
				                 .request()
				                 .put(Entity.entity(e, MediaType.APPLICATION_JSON));
				   System.out.println(response);
	}




	

}
