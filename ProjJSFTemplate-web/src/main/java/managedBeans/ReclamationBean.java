package managedBeans;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import reclamation.guru.services.ReclamationService;
import reclamation.guru.DTO.Reclamation;

@ManagedBean
@SessionScoped
public class ReclamationBean {
	private int Id;
	private String Nom;
	private String Prenom;
	private int Telephone;
	private String Type;
	private String Email;

	private List<Reclamation> Reclamation;
	private static final long serialVersionUID = 1L;

	ReclamationService E = new ReclamationService();

	public List<Reclamation> getReclamation() {
		Reclamation = E.GetAll();
		return Reclamation;
	}

	public void setReclamation(List<Reclamation> reclamation) {
		Reclamation = reclamation;
	}

	public String supprimer(Reclamation e) {
		E.Delete(e);
		return "/Reclamation/Index?faces-redirect=true";

	}

	public String addReclamation() {
		E.Create(new Reclamation(Nom, Prenom, Telephone, Type, Email

		));
		return "/Reclamation/Index?faces-redirect=true";

	}

	public String modifier(Reclamation e) throws IOException {

		this.setId(e.getId());
		this.setNom(e.getNom());
		this.setPrenom(e.getPrenom());
		this.setTelephone(e.getTelephone());
		this.setType(e.getType());
		this.setEmail(e.getEmail());

		System.out.println(e.getId());

		return "/Reclamation/Edit?faces-redirect=true";

	}

	public String MAJReclamation() {

		E.Update(Id, new Reclamation(Nom, Prenom, Telephone, Type, Email));
		Reclamation = E.GetAll();

		return "/Reclamation/Index?faces-redirect=true";
	}

	public int getId() {
		return Id;
	}

	public void setId(int reclamationId) {
		Id = reclamationId;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public String getPrenom() {
		return Prenom;
	}

	public void setPrenom(String prenom) {
		Prenom = prenom;
	}

	public int getTelephone() {
		return Telephone;
	}

	public void setTelephone(int telephone) {
		Telephone = telephone;
	}

	public String getType() {
		return Type;
	}

	public void setType(String type) {
		Type = type;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public ReclamationService getE() {
		return E;
	}

	public void setE(ReclamationService e) {
		E = e;
	}

}
